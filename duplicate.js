/*
請在一個正整數陣列中，找出第一個重複的數字
若陣列中無重複的數字，則回傳 -1
若陣列中有兩組以上重複的數字，則回傳所有第二個出現的數字中， index 最小的數字
請使用 JavasScript 或 TypeScript 作答
source code 請放在github或是任何git service

例：
a = [ 2, 1, 4, 5, 3, 2]
duplicate(a) = 2

b = [ 2, 4, 5, 1, 3]
duplicate(b) = -1

c = [ 2, 1, 3, 5, 3, 2] 
duplicate(c) = 3
*/

const duplicate = (arr) => {
    let firstDuplicatedIndex = null;
    const duplicatedNumbers = [];
    for (let index in arr) {
        const number = arr[index];
        if (parseInt(index) + 1 !== arr.length) {
            const subArr = arr.slice(parseInt(index) + 1, arr.length);
            for (let subIndex in subArr) {
                const n = subArr[subIndex];
                const i = 1 + parseInt(index) + parseInt(subIndex);
                if (number === n) {
                    if (duplicatedNumbers.indexOf(number) < 0) {
                        duplicatedNumbers.push(number);
                        if (firstDuplicatedIndex === null) {
                            firstDuplicatedIndex = i;
                        } else if (i < firstDuplicatedIndex) {
                            firstDuplicatedIndex = i;
                        }
                    }
                }
            }
        }
    }
    if (duplicatedNumbers.length > 0) {
        return arr[firstDuplicatedIndex];
    }
    return -1;
};

export default duplicate;

